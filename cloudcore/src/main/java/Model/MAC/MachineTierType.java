package Model.MAC;

public enum MachineTierType {
    S,
    T1,
    T2,
    G1,
    G2
}
