package Model.MAC;

import Model.GPU.GPU;
import Model.GPU.TierType;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Machine {
    private static long nextId = 0;

    private long id;
    private MachineTierType tierType;
    private MachineSubTierType subTierType;
    private MachineStateType stateType;
    private Integer cpuCount;
    private ArrayList<GPU> gpus;
    private LocalDateTime lastRan;
    private LocalDateTime terminated;

    public Machine(MachineTierType tierType, MachineSubTierType subTierType) {
        this.id = nextId;
        nextId++;

        this.tierType = tierType;
        this.subTierType = subTierType;
        this.stateType = MachineStateType.STOPPED;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MachineTierType getTierType() {
        return tierType;
    }

    public void setTierType(MachineTierType tierType) {
        this.tierType = tierType;
    }

    public MachineSubTierType getSubTierType() {
        return subTierType;
    }

    public void setSubTierType(MachineSubTierType subTierType) {
        this.subTierType = subTierType;
    }

    public MachineStateType getStateType() {
        return stateType;
    }

    public void setStateType(MachineStateType stateType) {
        this.stateType = stateType;
    }

    public Integer getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(Integer cpuCount) {
        this.cpuCount = cpuCount;
    }

    public ArrayList<GPU> getGpus() {
        return gpus;
    }

    public LocalDateTime getLastRan() {
        return lastRan;
    }

    public void setLastRan(LocalDateTime lastRan) {
        this.lastRan = lastRan;
    }

    public LocalDateTime getTerminated() {
        return terminated;
    }

    public void setTerminated(LocalDateTime terminated) {
        this.terminated = terminated;
    }

}
