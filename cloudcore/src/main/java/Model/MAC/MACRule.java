package Model.MAC;

import Model.GPU.TierType;

public class MACRule {
    private MachineTierType macType;
    private MachineSubTierType subMacType;
    private TierType gpuType;
    private int maxGPU;

    public MACRule(MachineTierType macType, MachineSubTierType subMacType, TierType gpuType, int maxGPU) {
        this.macType = macType;
        this.subMacType = subMacType;
        this.gpuType = gpuType;
        this.maxGPU = maxGPU;
    }

    public MachineTierType getMacType() {
        return macType;
    }

    public MachineSubTierType getSubMacType() {
        return subMacType;
    }

    public TierType getGpuType() {
        return gpuType;
    }

    public int getMaxGPU() {
        return maxGPU;
    }
}

