package Model.MAC;

public enum MachineSubTierType {
    MICRO,
    SMALL,
    MEDIUM,
    LARGE
}
