package Model.MAC;

public enum MachineStateType {
    STOPPED,
    STARTING,
    RUNNING,
    STOPPING,
    TERMINATED
}
