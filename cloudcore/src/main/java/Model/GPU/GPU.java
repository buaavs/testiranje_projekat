package Model.GPU;

import Model.MAC.Machine;

import java.time.LocalDateTime;

public class GPU {
    private static long nextId = 0;

    private long id;
    private TierType tierType;
    private StateType stateType;
    private AttachedStateType attachedStateType;
    private Machine attachedTo;
    private LocalDateTime lastRan;
    private LocalDateTime lastStopped;

    public GPU(TierType tierType) {
        this.id = nextId;
        nextId++;
        this.tierType = tierType;
        this.stateType = StateType.STOPPED;
        this.attachedStateType = AttachedStateType.DETACHED;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TierType getTierType() {
        return tierType;
    }

    public void setTierType(TierType tierType) {
        this.tierType = tierType;
    }

    public StateType getStateType() {
        return stateType;
    }

    public void setStateType(StateType stateType) {
        this.stateType = stateType;
    }

    public AttachedStateType getAttachedStateType() {
        return attachedStateType;
    }

    public void setAttachedStateType(AttachedStateType attachedStateType) {
        this.attachedStateType = attachedStateType;
    }

    public Machine getAttachedTo() {
        return attachedTo;
    }

    public void setAttachedTo(Machine attachedTo) {
        this.attachedTo = attachedTo;
    }

    public LocalDateTime getLastRan() {
        return lastRan;
    }

    public void setLastRan(LocalDateTime lastRan) {
        this.lastRan = lastRan;
    }

    public LocalDateTime getLastStopped() {
        return lastStopped;
    }

    public void setLastStopped(LocalDateTime lastStopped) {
        this.lastStopped = lastStopped;
    }
}
