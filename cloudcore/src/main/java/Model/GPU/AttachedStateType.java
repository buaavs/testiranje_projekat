package Model.GPU;

public enum AttachedStateType {
    ATTACHED,
    DETACHED
}
