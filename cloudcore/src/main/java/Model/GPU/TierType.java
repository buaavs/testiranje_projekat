package Model.GPU;

public enum TierType {
    SINGLE(1) ,
    DOUBLE(2),
    CLUSTER(4);

    private int val;

    private TierType(int val){
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
