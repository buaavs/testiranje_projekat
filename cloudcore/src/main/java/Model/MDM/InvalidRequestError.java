package Model.MDM;

import Model.MAC.ErrorType;

public class InvalidRequestError extends Error{
    public InvalidRequestError() {
        super(ErrorType.INVALID_REQUEST_ERR, "Invalid request!");
    }
}
