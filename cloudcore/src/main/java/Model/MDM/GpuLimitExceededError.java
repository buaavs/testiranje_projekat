package Model.MDM;

import Model.GPU.TierType;
import Model.MAC.ErrorType;

public class GpuLimitExceededError extends Error{

    public GpuLimitExceededError(TierType gpuType) {
        super(ErrorType.GPU_LIMIT_EXCEEDED_ERR, "GPU limit is exceeded! Current number of GPUs is "+gpuType.getVal()+" of type "+gpuType.toString());
    }
}
