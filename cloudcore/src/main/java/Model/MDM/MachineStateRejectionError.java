package Model.MDM;

import Model.MAC.ErrorType;

public class MachineStateRejectionError extends Error{

    public MachineStateRejectionError(String status, String action) {
        super(ErrorType.MACHINE_STATE_REJECTION_ERR, "Resource is in "+status+" state and can't perform "+action+"!");
    }
}
