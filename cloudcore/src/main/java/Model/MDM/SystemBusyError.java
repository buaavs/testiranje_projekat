package Model.MDM;

import Model.MAC.ErrorType;

public class SystemBusyError extends Error {

    public SystemBusyError() {
        super(ErrorType.SYSTEM_BUSY_ERROR, "System can't handle your request at this time!");
    }
}
