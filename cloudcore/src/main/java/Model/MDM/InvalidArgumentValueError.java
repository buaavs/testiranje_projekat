package Model.MDM;

import Model.MAC.ErrorType;

public class InvalidArgumentValueError extends Error{

    public InvalidArgumentValueError(String argName, String argValue) {
        super(ErrorType.INVALID_ARGUMENT_VAL_ERR, "Field"+argName+"received invalid argument "+argValue+"!");
    }
}
