package Repositories;

import Model.MAC.Machine;
import Model.MAC.MachineStateType;

import java.util.ArrayList;

public class MachineRepository {
    private ArrayList<Machine> machines;

    public MachineRepository() {
        machines = new ArrayList<>();
    }

    public int countNotTerminated(){
        int x = 0;
        for(Machine m : machines){
            if(m.getStateType() != MachineStateType.TERMINATED){
                x++;
            }
        }
        return x;
    }

    public void addMachine(Machine m){
        machines.add(m);
    }

    public Machine getMachineById(long id){
        for(Machine m : machines){
            if(m.getId() == id){
                return m;
            }
        }
        return null;
    }
}
