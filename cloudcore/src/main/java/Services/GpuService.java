package Services;

import Model.GPU.AttachedStateType;
import Model.GPU.GPU;
import Model.GPU.StateType;
import Model.MAC.Machine;
import Model.MDM.InvalidRequestError;
import Repositories.MachineRepository;

import javax.swing.plaf.nimbus.State;
import java.util.Random;

public class GpuService {
    private MachineRepository repo;

    public GPU startGPU(long id, long macId) throws InterruptedException {
        Machine mac = repo.getMachineById(macId);

        if(mac == null){
            throw new InvalidRequestError();
        }

        GPU gpu = null;
        for(GPU g:mac.getGpus()){
            if(g.getId() == id){
                gpu = g;
            }
        }

        if(gpu == null || gpu.getStateType() != StateType.STOPPED || gpu.getAttachedStateType() != AttachedStateType.ATTACHED){
            throw new InvalidRequestError();
        }

        gpu.setStateType(StateType.RUNNING);
        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);

        return gpu;
    }

    public GPU stopGPU(long id, long macId) throws InterruptedException {
        Machine mac = repo.getMachineById(macId);

        if(mac == null){
            throw new InvalidRequestError();
        }

        GPU gpu = null;
        for(GPU g:mac.getGpus()){
            if(g.getId() == id){
                gpu = g;
            }
        }

        if(gpu == null || gpu.getStateType() != StateType.RUNNING || gpu.getAttachedStateType() != AttachedStateType.ATTACHED){
            throw new InvalidRequestError();
        }

        gpu.setStateType(StateType.STOPPED);
        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);

        return gpu;
    }

}
