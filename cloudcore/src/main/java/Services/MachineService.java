package Services;

import Model.GPU.AttachedStateType;
import Model.GPU.GPU;
import Model.GPU.StateType;
import Model.GPU.TierType;
import Model.MAC.*;
import Model.MDM.*;
import Model.MDM.Error;
import Repositories.MachineRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class MachineService {
    private ArrayList<MACRule> rules;
    private MachineRepository repo;
    private GpuService gpuService = new GpuService();

    public MachineService() {
        rules = new ArrayList<>();
        gpuService = new GpuService();
        populateRules();
    }

    private void populateRules(){
        rules.add(new MACRule(MachineTierType.S,MachineSubTierType.MICRO,null,0));
        rules.add(new MACRule(MachineTierType.T1,MachineSubTierType.SMALL,null,0));
        rules.add(new MACRule(MachineTierType.T1,MachineSubTierType.MEDIUM,null,0));
        rules.add(new MACRule(MachineTierType.T2,MachineSubTierType.MEDIUM,null,0));
        rules.add(new MACRule(MachineTierType.T2,MachineSubTierType.LARGE,null,0));
        rules.add(new MACRule(MachineTierType.G1,MachineSubTierType.LARGE,TierType.SINGLE,1));
        rules.add(new MACRule(MachineTierType.G2,MachineSubTierType.LARGE,TierType.DOUBLE,2));
        rules.add(new MACRule(MachineTierType.G2,MachineSubTierType.LARGE,TierType.CLUSTER,4));
        rules.add(new MACRule(MachineTierType.G1,MachineSubTierType.LARGE,null,0));
        rules.add(new MACRule(MachineTierType.G2,MachineSubTierType.LARGE,null,2));
        rules.add(new MACRule(MachineTierType.G2,MachineSubTierType.LARGE,null,4));
    }

    public void validateMachine(MachineTierType macType, MachineSubTierType macSubType, TierType gpuType, int gpuCount) throws Error{
        ArrayList<MACRule> filtered = new ArrayList<>(rules);
        ArrayList<MACRule> forRemoval = new ArrayList<>();

        for(MACRule r : filtered){
            if( r.getMacType() != macType ){
                forRemoval.add(r);
            }
        }
        filtered.removeAll(forRemoval);
        forRemoval.clear();

        if(filtered.isEmpty()) throw new InvalidArgumentValueError("MachineTierType",macType.toString());

        for(MACRule r : filtered){
            if( r.getSubMacType() != macSubType ){
                forRemoval.add(r);
            }
        }
        filtered.removeAll(forRemoval);
        forRemoval.clear();

        if(filtered.isEmpty()) throw new InvalidArgumentValueError("MachineSubTierType",macSubType.toString());

        for(MACRule r : filtered){
            if( r.getGpuType() != gpuType ){
                forRemoval.add(r);
            }
        }
        filtered.removeAll(forRemoval);
        forRemoval.clear();

        if(filtered.isEmpty()) throw new InvalidArgumentValueError("GPU.TierType", gpuType.toString());

        if(gpuCount < 0) throw new InvalidArgumentValueError("GPUCount", gpuCount + "");

        MACRule r = filtered.get(0);
        if(r.getMaxGPU() < gpuCount) throw new InvalidArgumentValueError("GPUCount", gpuCount + "");

    }

    public Machine createMachine(MachineTierType macType, MachineSubTierType macSubType) throws  InterruptedException{
        return createMachine(macType,macSubType,null,0);
    }

    public Machine createMachine(MachineTierType macType, MachineSubTierType macSubType, TierType gpuType, Integer gpuCount) throws InterruptedException{
        validateMachine(macType,macSubType,gpuType,gpuCount);

        if(repo.countNotTerminated() >= 20){
            throw new SystemBusyError();
        }

        Machine machine = new Machine(macType,macSubType);
        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);
        repo.addMachine(machine);

        for(int i=0;i<gpuCount;i++){
            attachGPU(machine.getId(),gpuType);
        }

        return machine;
    }

    public Machine startMachine(long id) throws InterruptedException{
        Machine mac = repo.getMachineById(id);

        if(mac == null || mac.getStateType() != MachineStateType.STOPPED){
            throw new InvalidRequestError();
        }

        for(GPU g : mac.getGpus()){
            if(g.getStateType() == StateType.RUNNING){
                System.exit(0);
            }
        }

        mac.setStateType(MachineStateType.STARTING);

        try {
            for(GPU g : mac.getGpus()){
                gpuService.startGPU(g.getId(),mac.getId());
            }
        }catch (Error err){
            mac.setStateType(MachineStateType.STOPPED);
            return mac;
        }

        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);
        mac.setStateType(MachineStateType.RUNNING);
        return mac;
    }

    public Machine stopMachine(long id) throws InterruptedException{
        Machine mac = repo.getMachineById(id);

        if(mac == null){
            throw new InvalidRequestError();
        }

        if(mac.getStateType() != MachineStateType.RUNNING){
            throw new MachineStateRejectionError(mac.getStateType().toString(),"STOP");
        }

        mac.setStateType(MachineStateType.STOPPING);

        try {
            for(GPU g : mac.getGpus()){
                gpuService.stopGPU(g.getId(),mac.getId());
            }
        }catch (Error err){
            mac.setStateType(MachineStateType.TERMINATED);
            for(GPU g : mac.getGpus()){
                g.setStateType(StateType.STOPPED);
            }
            return mac;
        }



        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);
        mac.setStateType(MachineStateType.STOPPED);

        return mac;
    }

    public Machine attachGPU(long id, TierType gpuType) throws InterruptedException {
        Machine mac = repo.getMachineById(id);

        if(mac == null){
            throw new InvalidRequestError();
        }

        validateMachine(mac.getTierType(),mac.getSubTierType(),gpuType,0);

        Set<TierType> gpuSet = new HashSet<>();
        gpuSet.add(gpuType);

        for(GPU g : mac.getGpus()){
            gpuSet.add(g.getTierType());
        }

        if(gpuSet.size() > 1){
            throw new InvalidRequestError();
        }

        if(mac.getGpus().size() >= gpuType.getVal()){
            throw new GpuLimitExceededError(gpuType);
        }

        if(mac.getStateType() != MachineStateType.RUNNING || mac.getStateType() != MachineStateType.STOPPED){
            throw new MachineStateRejectionError(mac.getStateType().toString(),"AttachGPU");
        }

        MachineStateType initialState = mac.getStateType();

        if(initialState == MachineStateType.RUNNING){
            stopMachine(mac.getId());
        }

        for(GPU g : mac.getGpus()){
            if(g.getStateType() != StateType.STOPPED){
                System.exit(0);
            }
        }

        GPU g = new GPU(gpuType);

        mac.getGpus().add(g);

        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);

        g.setAttachedStateType(AttachedStateType.ATTACHED);
        if(initialState == MachineStateType.RUNNING){
            startMachine(mac.getId());
        }

        return mac;
    }

    public Machine deattachGPU(long id) throws InterruptedException {
        Machine mac = repo.getMachineById(id);

        if(mac == null || mac.getGpus().size() == 0){
            throw new InvalidRequestError();
        }

        if(mac.getStateType() != MachineStateType.RUNNING || mac.getStateType() != MachineStateType.STOPPED){
            throw new MachineStateRejectionError(mac.getStateType().toString(),"AttachGPU");
        }

        MachineStateType initialState = mac.getStateType();

        if(initialState == MachineStateType.RUNNING){
            stopMachine(mac.getId());
        }

        for(GPU g : mac.getGpus()){
            if(g.getStateType() != StateType.STOPPED){
                System.exit(0);
            }
        }

        GPU g = mac.getGpus().get(mac.getGpus().size()-1);
        mac.getGpus().remove(mac.getGpus().size()-1);

        Random rand = new Random();
        Thread.sleep((rand.nextInt(15)-5)*1000);

        g.setAttachedStateType(AttachedStateType.DETACHED);

        if(initialState == MachineStateType.RUNNING){
            startMachine(mac.getId());
        }

        return mac;
    }

    public Machine restartMachine(long id) throws InterruptedException {
        Machine mac = repo.getMachineById(id);

        if(mac == null){
            throw new InvalidRequestError();
        }

        if(mac.getStateType() != MachineStateType.RUNNING){
            throw new MachineStateRejectionError(mac.getStateType().toString(),"RESTART");
        }

        stopMachine(mac.getId());

        startMachine(mac.getId());

        return mac;
    }
}